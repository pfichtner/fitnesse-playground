package pfichtner.github.com.fitnesseplayground;

import pfichtner.github.com.fitnesseplayground.Calculator;
import pfichtner.github.com.fitnesseplayground.Result;
import lombok.Delegate;
import lombok.Setter;

@Setter
public class BildeSummeUndProdukt {

	private int a, b;

	@Delegate
	private final Calculator calculator = new Calculator();

	@Delegate
	private Result result;

	public void execute() {
		this.result = calculator.calcualte(this.a, this.b);
	}

	public int sum() {
		return result.getSum();
	}

	public int product() {
		return result.getProduct();
	}

	public int sub() {
		return result.getSub();
	}

}
