package pfichtner.github.com.fitnesseplayground;

import org.junit.runner.RunWith;

import fitnesse.junit.FitNesseSuite;
import fitnesse.junit.FitNesseSuite.DebugMode;
import fitnesse.junit.FitNesseSuite.FitnesseDir;
import fitnesse.junit.FitNesseSuite.Name;
import fitnesse.junit.FitNesseSuite.OutputDir;

@RunWith(FitNesseSuite.class)
@Name("TestSuite")
@FitnesseDir("FitNesseRoot")
@OutputDir("target/fitnesse-results")
@DebugMode(true)
public class FitnesseJunit {

	public void test() {
		// do nothing
	}

}
