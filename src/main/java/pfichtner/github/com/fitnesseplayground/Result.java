package pfichtner.github.com.fitnesseplayground;

import lombok.Data;

@Data
public class Result {

	private final int sum, product, sub;

}
